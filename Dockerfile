FROM python:3.8

WORKDIR /home
ENV TZ=Europe/Moscow

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
#EXPOSE host_port

CMD ["python", "server.py"]
